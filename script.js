
let jsonDownloaded;

function displayIHM()
{
    /* button submit url */
    const form = document.getElementById('submit-modal');
    form.addEventListener("click", downloadJson);

    /* boutton valider de la modale */
    const btn = document.getElementById('modal-btn');
    btn.addEventListener("click", downloadCsv);
}

function displayModal(json){
    Object.keys(json[0]).forEach(key => {

        /* let form = document.getElementById('checkboxform'); */

        let container = document.getElementById('carte');

        //Créer row qui contient deux colonnes
        let row = document.createElement('div');
        row.setAttribute("class", "row align-items-center justify-content-center flex-row rowcheckbox");
        container.appendChild(row);
        
        //Créer deux colonnes qui contiennent respectivement <input> et <label>
        let colInput = document.createElement('div');
        colInput.setAttribute("class", "col-2 text-center from-check");
        row.appendChild(colInput);
        
        let colLabel = document.createElement('div');
        colLabel.setAttribute("class", "col-10 text-center from-check");
        row.appendChild(colLabel);

        //Créer bouton <input>
        let input = document.createElement('input');
        input.setAttribute("class", "form-check-input checkbox");
        input.setAttribute("type", "checkbox");
        input.setAttribute("checked", "false");
        input.setAttribute("id", key + "-checkbox");
        colInput.appendChild(input);

        //Créer <label>
        let label = document.createElement('label');
        label.setAttribute("class", "form-check-label");
        label.setAttribute("for", key + "-checkbox");
        label.innerHTML = key;
        colLabel.appendChild(label);
    });
}

function downloadCsv(){
    const csvDataType = "data:text/csv;charset=utf-8,";
    let csv = "";
    let csvHeaders = "";

    //Réinitialise le champ input du url form
    let form = document.getElementById('urlForm');
    form.inputUrl.value = null;

    //Formate le json selon les choix de l'utilisateur
    let json = cutJson(jsonDownloaded);

    //Composition du corps du csv
    json.forEach(arr => {
        Object.keys(arr).forEach(key => {
            csv += arr[key] + ";";
        });
        csv += "\n";
    });

    //La string csvHeaders est composée des clés du json et sera insérée à la première ligne du csv pour représenter les titres de colonnes
    Object.keys(json[0]).forEach(key => {
        csvHeaders = csvHeaders + key + ";";
    });
    csvHeaders += "\n";

    //Composition du csv
    csv = `${csvDataType}` + `${csvHeaders}` + csv;

    const encodedUri = encodeURI(csv);
    window.open(encodedUri);
}

/**
 * Supprime les champs selon les choix de l'utilisateurs via une checkbox
 * @param {*} json 
 */
function cutJson(json){
    /* let inputArr = document.querySelectorAll(".checkbox"); */
    
    json.forEach(jsonElement => {   
        Object.keys(jsonElement).forEach(key => {
            let input = document.getElementById(key + '-checkbox');
            if(!input.checked)
                delete jsonElement[key];
        });
    });
    
    console.log(json);
    return json;
}

async function downloadJson(event){
    event.preventDefault();
    let table = await getJson();
    flat(table);

    jsonDownloaded = table;
    displayModal(table); 
}

/**
 * Transforme un json multidimensionnel en un json unidimensionnel
 * @param {*} table 
 */
async function flat(table){
    for(let i in table){
        for(let j in table[i]){
            if(table[i][j] instanceof Object){
 
                let objRemoved = table[i][j];
                delete table[i][j];

                const objRemovedKeys = Object.keys(objRemoved);
                objRemovedKeys.forEach(key => {
                    //ToDo: gérer les keys en double pour éviter qu'elles s'écrasent
                    table[i][key] = objRemoved[key];
                });

                flat(table);
            }
        }
    }
}

function convertJsonToArray(json){
    arr=[];
    for(let objElement in json){
        arr[objElement] = json[objElement];
    }
    
    return arr;
}

async function getJson(){
    const form = document.getElementById('urlForm');
    const uri = form.inputUrl.value;

    const table = await fetch(uri)
    .then((response) => response.json())
    .then(json => json)
    .catch((error)=>{
        console.error(error);
    });

    return table;
}

//***** affichage *****

displayIHM();
